## 1

### RTFM and LMGTFY

In the first bullet point of this document ![README](https://gitlab.com/edmartt/new-developer_v3/-/blob/edmartt/chore/answers/README.md?ref_type=heads) I have been asked to create a document in which I relate when I last used "LMGTFY" and "RTFM".

I can assure you that as a long time user of forums and communities, they are two acronyms that have helped me to advance a lot in my learning, since they encourage self-teaching. I am always using them, they are very useful to me as a personal philosophy, since manuals and research are a fundamental part of problem solving. What is our problem today has probably already been someone else's problem. The most recent occasion in which I have used these "principles" if you can call them this way, has been in this test, because I had to learn some things from gitlab to be able to do it and the tool that was most useful to me is the documentation.

As a note, I would suggest some caution when trying to use this with others, as it can be misunderstood and interpreted as disrespect in some environments.

#### OS

I'm mostly a Linux user, Debian and Fedora are the distributions I use the most, although I can also get along with Windows and I've had the chance to be a MacOS user for a year.


#### Languages

The languages I have used the most during the last 5 years have been Python and Go with some frameworks like Django and Flask for Python as well as Gin and Echo or Go. I have also had some experience with Java and C which are the ones that have helped me with my fundamentals since I started.

## 2

In this repository I have uploaded a mini project in which I implement CI:

![repository](https://gitlab.com/edmartt/python-password-hasher)

It should be noted that using CI contributes to productivity and reduces the number of errors that usually occur when a more manual approach is used. In the example code we can see how the unit tests have been automated, the installation of dependencies and also the coverage of the tests, adding also that the version of the interpreter in which the project has been developed is specified.

## 3

#### About my Portfolio

This is one of the projects I have developed in a public way:


![gRPC-HTTP project](https://gitlab.com/edmartt/grpc-crud)

This project makes use of technologies such as gRPC, Swagger(OpenAPI now), Docker, SQLite and HTTP. Besides that I make use of concurrency, SOLID principles and unit testing.

And this is another one:

![daily task backend](https://gitlab.com/Edmartt/django-task-backend)

I make use of technologies such as Python, Django, JWT, Swagger (now OpenAPI), CI/CD helped by Github Actions, Docker and Kubernetes.

## 4

The solution is here: ![here](https://gitlab.com/edmartt/new-developer_v3/-/tree/edmartt/chore/answers/task_4?ref_type=heads)

## 5

Added to Google Chat