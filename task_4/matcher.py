
def match_finder(number_list: list, expected: int) -> list | str:

    for main_index in range(len(number_list)):

        for second_index in range(main_index+1, len(number_list)):

            if number_list[main_index] + number_list[second_index] == expected:

                result = [number_list[main_index], number_list[second_index]]
                return result

    return "Not Found"


'''numbers = [5, 7, 12, 13, 15]

result = match_finder(numbers, 12)

print("Pair: ", result)'''
