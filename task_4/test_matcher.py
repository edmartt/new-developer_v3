import unittest
from matcher import match_finder


class TestMatchPair(unittest.TestCase):

    def test_expected_value_found(self):

        number_list = [5, 7, 9, 13, 15]

        result = match_finder(number_list, 12)

        self.assertEqual(result, [5, 7])

    def test_number_list_empty(self):

        number_list = []

        result = match_finder(number_list, 9)

        self.assertEqual(result, "Not Found")

    def test_expected_value_not_int(self):

        expected = "19"

        self.assertNotIsInstance(expected, int)

    def test_number_collections_not_list(self):

        number_list = (1, 4, 6, 7, 8)

        self.assertNotIsInstance(number_list, list)


if __name__ == '__main__':
    unittest.main(verbosity=2)
